# docker-node-http-server

A simple apline-based [docker image](https://hub.docker.com/r/samcarpentier/packer-ansible/) with Node http-server that can be used to serve static web pages.

## Helpful link for Dockerfile implementation

http://mowson.org/karl/2016/2016-05-22_install_ansible_under_alpinelinux/
